#ifndef CATEGORIA_H
#define CATEGORIA_H

typedef struct s_cat{
    char *nome;
    int count;
}* CATEGORIA;

CATEGORIA initCat(char *nome, int count);
int comparaCatAVL (const void *avl_a, const void *avl_b,
                                 void *avl_param);
void freeCatAVL(void *avl_item, void *avl_param);
void aumentaCounter(CATEGORIA cat, int aumento);

#endif

