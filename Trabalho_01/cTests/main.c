#include <stdio.h>
#include <stdlib.h>
#include "avl.h"
#include "categoria.h"

void printTree(AVL avl);

int main(int argc, char** argv) {
    // Categoria usada para iterador
    CATEGORIA procura;
    // Inicializa categorias exemplo
    CATEGORIA cat1 = initCat("Publicacao", 1);
    CATEGORIA cat2 = initCat("Tese", 1);
    
    // Cria arvore
    AVL avl = avl_create (comparaCatAVL, NULL, NULL);
    
    //Insere categorias de exemplo na arvore
    avl_insert(avl, cat1);
    avl_insert(avl, cat2);
    
    //Procura a categoria com nome "Publicacao" e adiciona 1 ao contador
    if((procura = avl_find(avl, cat1)) != NULL){
        aumentaCounter(procura, 1);
    }
    
    // Faz print à árvore.
    printTree(avl);
    
    //Novo print à árvore para testar que o free ao iterador nao afectou
    // a arvore
    printTree(avl);
    
    // Liberta memoria da árvore
    avl_destroy(avl, freeCatAVL);
    return (EXIT_SUCCESS);
}

void printTree(AVL avl){
    CATEGORIA cat;
    ITERADOR it = avl_new_it(avl);
    
    while((cat = (CATEGORIA) avl_t_next(it)) != NULL){
        printf("CATEGORIA{Nome=%s, Count=%d}\n", cat->nome, cat->count);
    }
    
    avl_t_free(it);
}
