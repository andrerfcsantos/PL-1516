#ifndef ATRIBUTO_H
#define ATRIBUTO_H
#include "Atributo.h"

struct s_atributo{
    char *nome;
    char *tipo;
};

typedef struct s_atributo *ATRIBUTO;

ATRIBUTO novoAtributoVazio(void);
ATRIBUTO novoAtributoNome(char *nome);
ATRIBUTO novoAtributo(char *nome, char *tipo);
void setNomeAtributo(ATRIBUTO a, char *novoNome);
void setTipoAtributo(ATRIBUTO a, char *novoTipo);
void freeAtributo(ATRIBUTO a);
void freeAtributoAVL(void *avl_item, void *avl_param);
int comparaAtrAVL(const void *avl_a, const void *avl_b, void *avl_param);
void printAtributo(ATRIBUTO a);

#endif

