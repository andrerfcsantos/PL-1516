#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Atributo.h"



ATRIBUTO novoAtributoVazio() {
    ATRIBUTO novoAtributo = (ATRIBUTO) malloc(sizeof (struct s_atributo));
    return novoAtributo;
}

ATRIBUTO novoAtributoNome(char *nome) {

    ATRIBUTO novoAtributo = (ATRIBUTO) malloc(sizeof (struct s_atributo));

    if (novoAtributo != NULL) {
        novoAtributo->nome = strdup(nome);
        novoAtributo->tipo = NULL;
    }

    return novoAtributo;
}

ATRIBUTO novoAtributo(char *nome, char *tipo) {

    ATRIBUTO novoAtributo = (ATRIBUTO) malloc(sizeof (struct s_atributo));

    if (novoAtributo != NULL) {
        novoAtributo->nome = strdup(nome);
        novoAtributo->tipo = strdup(tipo);
    }

    return novoAtributo;
}

void setNomeAtributo(ATRIBUTO a, char *novoNome) {
    if (a != NULL) {
        free(a->nome);
        a->nome = strdup(novoNome);
    }
}

void setTipoAtributo(ATRIBUTO a, char *novoTipo) {
    if (a != NULL) {
        free(a->tipo);
        a->tipo = strdup(novoTipo);
    }
}

void freeAtributo(ATRIBUTO a){
    if(a!=NULL){
        free(a->tipo);
        free(a->nome);
    }
    free(a);
}

void freeAtributoAVL(void *avl_item, void *avl_param){
    ATRIBUTO a = (ATRIBUTO) avl_item;
    if(a!=NULL) freeAtributo(a);
}

int comparaAtrAVL (const void *avl_a, const void *avl_b,
                                 void *avl_param){
    ATRIBUTO a = (ATRIBUTO) avl_a;
    ATRIBUTO b = (ATRIBUTO) avl_b;
    
    return strcmp(a->nome, b->nome);
}

void printAtributo(ATRIBUTO a){
    printf("Atributo{nome: %s | tipo: %s}", a->nome, a->tipo);
}



