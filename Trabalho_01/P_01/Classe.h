#ifndef CLASSE_H
#define CLASSE_H
#include "avl.h"
#include "Classe.h"
#include "Associacao.h"
#include "Atributo.h"

struct s_classe{
    char *nome;
    AVL associacoes;
    AVL atributos;
};

typedef struct s_classe *CLASSE;

CLASSE novaClasseVazia(void);
CLASSE novaClasse(char *nome);
void setNomeClasse(CLASSE c, char *novoNome);
ASSOCIACAO existeAssociacaoClasse(CLASSE c, char *classeDest);
void addAssociacaoToClasse(CLASSE c, char *classeDest, char *label);
ATRIBUTO existeAtributoClasse(CLASSE c, char *nomeAtributo);
int nAtributos(CLASSE c);
void addAtributoToClasse(CLASSE c, char *nomeAtributo, char *tipoAtributo);
void freeClasseAVL(void *avl_item, void *avl_param);
void freeClasse(CLASSE c);
int comparaClasseAVL(const void *avl_a, const void *avl_b, void *avl_param);
void printClasse(CLASSE c);

#endif

