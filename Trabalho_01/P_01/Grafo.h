#ifndef GRAFO_H
#define GRAFO_H
#include "avl.h"
#include "Grafo.h"
#include "Classe.h"

struct s_grafo{
    AVL classes;
};

typedef struct s_grafo *GRAFO;

GRAFO novoGrafo(void);
void addClasseToGrafo(GRAFO g, char *nomeClasse);
CLASSE existeClasseGrafo(GRAFO g, char *nomeClasse);
void addAssociacaoToGrafo(GRAFO g, char *classeOrigem, char *classeDestino, char *labelAssoc);
void addAtributoToGrafo(GRAFO g, char *nomeClasse, char *atributo, char *tipoAtributo);
void freeGrafo(GRAFO g);
void printGrafo(GRAFO g);

#endif

