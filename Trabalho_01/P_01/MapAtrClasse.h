#ifndef MAPATRCLASSE_H
#define MAPATRCLASSE_H
#include "avl.h"


struct s_mapatrclasse{
    AVL atributos;
};

typedef struct s_mapatrclasse *MAP_ATR_CLASSE;

MAP_ATR_CLASSE novoMapAtrClasse(void);
void addAtrClasseToMap(MAP_ATR_CLASSE m, char *atributo, char *classe);
char *getClasseAtr(MAP_ATR_CLASSE m, char *atributo);
void freeMapAtrClasse(MAP_ATR_CLASSE m);

#endif

