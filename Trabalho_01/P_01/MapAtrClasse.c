#include <stdio.h>
#include <stdlib.h>
#include "MapAtrClasse.h"
#include "AtrClasse.h"

MAP_ATR_CLASSE novoMapAtrClasse() {
    MAP_ATR_CLASSE res = (MAP_ATR_CLASSE) malloc(sizeof (struct s_mapatrclasse));
    res->atributos = avl_create(comparaAtrClasseAVL, NULL, NULL);
    return res;
}

void addAtrClasseToMap(MAP_ATR_CLASSE m, char *atributo, char *classe){
    ATRCLASSE atProcura;
    ATRCLASSE at = novoAtrClasse(atributo, classe);
    if((atProcura = avl_insert(m->atributos,at)) != NULL)
        freeAtrClasse(at);
}

char *getClasseAtr(MAP_ATR_CLASSE m, char *atributo){
    char *res = NULL;
    ATRCLASSE a = novoAtrClasseAtributo(atributo);
    ATRCLASSE proc = avl_find(m->atributos, a);
    if(proc!=NULL) res = proc->classe;
    freeAtrClasse(a);
    return res;
}

void freeMapAtrClasse(MAP_ATR_CLASSE m) {
    if (m != NULL) avl_destroy(m->atributos, freeAtrClasseAVL);
    free(m);
}




