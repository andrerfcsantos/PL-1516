#ifndef ASSOCIACAO_H
#define ASSOCIACAO_H

#include "Associacao.h"

struct s_associacao{
    char *nomeClasseDestino;
    char *labelAssociacao;
};

typedef struct s_associacao *ASSOCIACAO;

ASSOCIACAO novaAssociacaoVazia(void);
ASSOCIACAO novaAssociacaoDestino(char * classeDestino);
ASSOCIACAO novaAssociacao(char *classeDestino, char *label);
void setClasseDestinoAssociacao(ASSOCIACAO a, char *novaClassDest);
void setLabelAssociacao(ASSOCIACAO a, char *novaLabel);
void freeAssociacao(ASSOCIACAO a);
void freeAssociacaoAVL(void *avl_item, void *avl_param);
int comparaAssocAVL(const void *avl_a, const void *avl_b, void *avl_param);
void printAssociacao(ASSOCIACAO a);


#endif

