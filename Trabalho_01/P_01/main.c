#include <stdio.h>
#include <stdlib.h>
#include "avl.h"
#include "Atributo.h"
#include "Classe.h"
#include "Grafo.h"
#include "Associacao.h"

int main(int argc, char** argv) {
    GRAFO grafo = novoGrafo();
    
    //Adiciona classes
    addClasseToGrafo(grafo, "Coordenadas");
    addClasseToGrafo(grafo, "CoordenadasPolares");
    addClasseToGrafo(grafo, "CoordenadasEsfericas");
    
    //Adiciona atributos às classes
    addAtributoToGrafo(grafo, "CoordenadasEsfericas", "raio", "float");
    addAtributoToGrafo(grafo, "CoordenadasEsfericas", "anguloAzimuth", "float");
    addAtributoToGrafo(grafo, "CoordenadasEsfericas", "anguloPolar", "float");
    
    addAtributoToGrafo(grafo, "CoordenadasPolares", "raio", "float");
    addAtributoToGrafo(grafo, "CoordenadasPolares", "anguloPolar", "float");
    
    //Adiciona associacoes
    addAssociacaoToGrafo(grafo, "CoordenadasPolares", "Coordenadas", "<<subclass>>");
    addAssociacaoToGrafo(grafo, "CoordenadasEsfericas", "Coordenadas", "<<subclass>>");
    
    printGrafo(grafo);
    freeGrafo(grafo);
    return (EXIT_SUCCESS);
}

