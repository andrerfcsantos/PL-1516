#include <stdio.h>
#include <stdlib.h>
#include "avl.h"
#include "Grafo.h"
#include "Classe.h"
#include "Atributo.h"
#include "Associacao.h"

GRAFO novoGrafo(){
    GRAFO g = (GRAFO) malloc(sizeof(struct s_grafo));
    g->classes = avl_create(comparaClasseAVL, NULL, NULL);
    return g;
}

void addClasseToGrafo(GRAFO g, char *nomeClasse){
    CLASSE c = novaClasse(nomeClasse);
    avl_insert(g->classes, c);
}


CLASSE existeClasseGrafo(GRAFO g, char *nomeClasse){
    CLASSE cl = novaClasse(nomeClasse);
    CLASSE res = (CLASSE) avl_find(g->classes, cl);
    freeClasse(cl);
    return res;
}

void addAssociacaoToGrafo(GRAFO g, char *classeOrigem, char *classeDestino, 
                                                char *labelAssoc){
    ASSOCIACAO a;
    CLASSE cl;
    
    cl = existeClasseGrafo(g, classeOrigem);
    
    if(cl == NULL){
        cl = novaClasse(classeOrigem);
        avl_insert(g->classes, cl);
    }
    
    a = existeAssociacaoClasse(cl, classeDestino);
    
    if(a == NULL){
        addAssociacaoToClasse(cl, classeDestino, labelAssoc);
    }
    
}

void addAtributoToGrafo(GRAFO g, char *nomeClasse, char *atributo, char *tipoAtributo){
    ATRIBUTO at;
    CLASSE cl;
    
    cl = existeClasseGrafo(g, nomeClasse);
    
    if(cl == NULL){
        cl = novaClasse(nomeClasse);
        avl_insert(g->classes, cl);
    }
    
    at = existeAtributoClasse(cl, atributo);
    
    if(at == NULL){
        addAtributoToClasse(cl, atributo, tipoAtributo);
    }
    
    
}

void freeGrafo(GRAFO g){
    if(g!=NULL) avl_destroy(g->classes, freeClasseAVL);
    free(g);
}

void printGrafo(GRAFO g){
    ITERADOR itClasses = avl_new_it(g->classes);
    CLASSE c;
    
    while((c = avl_t_next(itClasses))!=NULL){
        printClasse(c);
        printf("\n");
    }
    
    avl_t_free(itClasses);
}



