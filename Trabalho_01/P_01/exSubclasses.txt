<SubClassOf>
    <Class IRI="Carro"/>
    <Class IRI="Veiculo"/>
</SubClassOf>

<SubClassOf>
    <Class IRI="Bicicleta"/>
    <Class IRI="Veiculo"/>
</SubClassOf>

<DataPropertyDomain>
    <DataProperty IRI="#nrRodas"/>
    <Class IRI="#Veiculo"/>
</DataPropertyDomain>
<DataPropertyRange>
    <DataProperty IRI="#nrRodas"/>
    <Datatype abbreviatedIRI="int"/>
</DataPropertyRange>

<DataPropertyDomain>
    <DataProperty IRI="#modelo"/>
    <Class IRI="#Veiculo"/>
</DataPropertyDomain>
<DataPropertyRange>
    <DataProperty IRI="#modelo"/>
    <Datatype abbreviatedIRI="string"/>
</DataPropertyRange>

<DataPropertyDomain>
    <DataProperty IRI="#temCesto"/>
    <Class IRI="#Bicicleta"/>
</DataPropertyDomain>
<DataPropertyRange>
    <DataProperty IRI="#temCesto"/>
    <Datatype abbreviatedIRI="boolean"/>
</DataPropertyRange>

<DataPropertyDomain>
    <DataProperty IRI="#nrLugares"/>
    <Class IRI="#Carro"/>
</DataPropertyDomain>
<DataPropertyRange>
    <DataProperty IRI="#nrLugares"/>
    <Datatype abbreviatedIRI="int"/>
</DataPropertyRange>
