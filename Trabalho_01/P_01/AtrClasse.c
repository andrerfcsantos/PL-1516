#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AtrClasse.h"

ATRCLASSE novoAtrClasseVazio(void) {
    ATRCLASSE novoAtrClasse = (ATRCLASSE) malloc(sizeof (struct s_atrclasse));
    return novoAtrClasse;
}

ATRCLASSE novoAtrClasseAtributo(char *atributo) {

    ATRCLASSE novoAtributo = (ATRCLASSE) malloc(sizeof (struct s_atrclasse));

    if (novoAtributo != NULL) {
        novoAtributo->atributo = strdup(atributo);
        novoAtributo->classe = NULL;
    }

    return novoAtributo;
}

ATRCLASSE novoAtrClasse(char *atributo, char *classe) {
    ATRCLASSE novoAtributo = (ATRCLASSE) malloc(sizeof (struct s_atrclasse));

    if (novoAtributo != NULL) {
        novoAtributo->atributo = strdup(atributo);
        novoAtributo->classe = strdup(classe);
    }

    return novoAtributo;
}

void setAtributoAtrClasse(ATRCLASSE a, char *novoAtr) {
    if (a != NULL) {
        free(a->atributo);
        a->atributo = strdup(novoAtr);
    }
}

void setClasseAtrClasse(ATRCLASSE a, char *novaClasse) {
    if (a != NULL) {
        free(a->classe);
        a->classe = strdup(novaClasse);
    }
}

void freeAtrClasse(ATRCLASSE a) {
    if (a != NULL) {
        free(a->atributo);
        free(a->classe);
    }
    free(a);

}

void freeAtrClasseAVL(void *avl_item, void *avl_param) {
    ATRCLASSE a = (ATRCLASSE) avl_item;
    if (a != NULL) freeAtrClasse(a);
}

int comparaAtrClasseAVL(const void *avl_a, const void *avl_b, void *avl_param) {
    ATRCLASSE a = (ATRCLASSE) avl_a;
    ATRCLASSE b = (ATRCLASSE) avl_b;

    return strcmp(a->atributo, b->atributo);
}

void printAtrClasse(ATRCLASSE a) {
    printf("AtrClasse{atributo: %s | classe: %s}", a->atributo, a->classe);
}


