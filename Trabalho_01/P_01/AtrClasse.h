#ifndef ATRCLASSE_H
#define ATRCLASSE_H

struct s_atrclasse{
    char *classe;
    char *atributo;
};

typedef struct s_atrclasse * ATRCLASSE;

ATRCLASSE novoAtrClasseVazio(void);
ATRCLASSE novoAtrClasseAtributo(char *atributo);
ATRCLASSE novoAtrClasse(char *atributo, char *classe);
void setAtributoAtrClasse(ATRCLASSE a, char *novoAtr);
void setClasseAtrClasse(ATRCLASSE a, char *novaClasse);
void freeAtrClasse(ATRCLASSE a);
void freeAtrClasseAVL(void *avl_item, void *avl_param);
int comparaAtrClasseAVL(const void *avl_a, const void *avl_b, void *avl_param);
void printAtrClasse(ATRCLASSE a);


#endif

