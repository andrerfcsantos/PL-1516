#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Classe.h"
#include "Associacao.h"
#include "Atributo.h"
#include "avl.h"

CLASSE novaClasseVazia(){
    CLASSE novaClasse = (CLASSE) malloc(sizeof(struct s_classe));
    novaClasse->nome = (char *) malloc(sizeof(char));
    novaClasse->associacoes = avl_create(comparaAssocAVL, NULL, NULL);
    novaClasse->atributos = avl_create(comparaAtrAVL, NULL, NULL);
    return novaClasse;
}

CLASSE novaClasse(char *nome){
    CLASSE novaClasse = (CLASSE) malloc(sizeof(struct s_classe));
    
    if(novaClasse!=NULL){
        novaClasse->nome = strdup(nome);
        novaClasse->associacoes = avl_create(comparaAssocAVL, NULL, NULL);
        novaClasse->atributos = avl_create(comparaAtrAVL, NULL, NULL);
    }
    
    return novaClasse;
}

void setNomeClasse(CLASSE c, char *novoNome){
    if(c!=NULL){
        free(c->nome);
        c->nome = strdup(novoNome);
    }
}

ASSOCIACAO existeAssociacaoClasse(CLASSE c, char *classeDest){
    ASSOCIACAO assoc = novaAssociacaoDestino(classeDest);
    ASSOCIACAO res = (ASSOCIACAO) avl_find(c->associacoes, assoc);
    freeAssociacao(assoc);
    return res;
}

void addAssociacaoToClasse(CLASSE c, char *classeDest, char* label){
    ASSOCIACAO assoc = novaAssociacao(classeDest, label);
    if(avl_insert(c->associacoes, assoc)!=NULL)
        freeAssociacao(assoc);
}

ATRIBUTO existeAtributoClasse(CLASSE c, char *nomeAtributo){
    ATRIBUTO atr = novoAtributoNome(nomeAtributo);
    ATRIBUTO res = (ATRIBUTO) avl_find(c->atributos, atr);
    freeAtributo(atr);
    return res;
}

int nAtributos(CLASSE c){
    return avl_count(c->atributos);
}

void addAtributoToClasse(CLASSE c, char *nomeAtributo, char *tipoAtributo){
    ATRIBUTO atr = novoAtributo(nomeAtributo, tipoAtributo);
    if(avl_insert(c->atributos, atr)!=NULL)
        freeAtributo(atr);
}

void freeClasseAVL(void *avl_item, void *avl_param){
    CLASSE a = (CLASSE) avl_item;
    if(a != NULL) freeClasse(a);
}

void freeClasse(CLASSE c){
    if(c != NULL){
        free(c->nome);
        avl_destroy(c->associacoes, freeAssociacaoAVL);
        avl_destroy(c->atributos, freeAtributoAVL);
    }
    free(c);
}

int comparaClasseAVL (const void *avl_a, const void *avl_b,
                                 void *avl_param){
    CLASSE a = (CLASSE) avl_a;
    CLASSE b = (CLASSE) avl_b;
    
    return strcmp(a->nome, b->nome);
}

void printClasse(CLASSE c){
    ATRIBUTO a;
    ASSOCIACAO assoc;
    ITERADOR itAtr, itAssoc;
    printf("Classe {");
    
    printf("nome = %s, ", c->nome);
    
    itAtr = avl_new_it(c->atributos);
    
    while((a = (ATRIBUTO) avl_t_next(itAtr))!=NULL){
        printAtributo(a);
        printf(", ");
    }
    itAssoc = avl_new_it(c->associacoes);
    
    while((assoc = (ASSOCIACAO) avl_t_next(itAssoc))!=NULL){
        printAssociacao(assoc);
        printf(",");
    }
    
    avl_t_free(itAtr);
    avl_t_free(itAssoc);
    
    printf("}");
}