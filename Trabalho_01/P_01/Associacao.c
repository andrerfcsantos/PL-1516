#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Associacao.h"

ASSOCIACAO novaAssociacaoVazia() {
    ASSOCIACAO novaAssoc = (ASSOCIACAO) malloc(sizeof (struct s_associacao));
    return novaAssoc;
}

ASSOCIACAO novaAssociacaoDestino(char * classeDestino) {

    ASSOCIACAO novaAssoc = (ASSOCIACAO) malloc(sizeof (struct s_associacao));

    if (novaAssoc != NULL) {
        novaAssoc->nomeClasseDestino = strdup(classeDestino);
        novaAssoc->labelAssociacao = NULL;
    }

    return novaAssoc;
}

ASSOCIACAO novaAssociacao(char * classeDestino, char *label) {

    ASSOCIACAO novaAssoc = (ASSOCIACAO) malloc(sizeof (struct s_associacao));

    if (novaAssoc != NULL) {
        novaAssoc->nomeClasseDestino = strdup(classeDestino);
        novaAssoc->labelAssociacao = strdup(label);
    }

    return novaAssoc;
}

void setClasseDestinoAssociacao(ASSOCIACAO a, char *novaClassDest) {
    if (a != NULL) {
        free(a->nomeClasseDestino);
        a->nomeClasseDestino = strdup(novaClassDest);
    }
}

void setLabelAssociacao(ASSOCIACAO a, char *novaLabel) {
    if (a != NULL) {
        free(a->labelAssociacao);
        a->labelAssociacao = strdup(novaLabel);
    }
}

void freeAssociacao(ASSOCIACAO a){
    if(a!=NULL){
        free(a->labelAssociacao);
        free(a->nomeClasseDestino);
    }
    free(a);
}

void freeAssociacaoAVL(void *avl_item, void *avl_param){
    ASSOCIACAO a = (ASSOCIACAO) avl_item;
    if(a!=NULL) freeAssociacao(a);
}

int comparaAssocAVL (const void *avl_a, const void *avl_b,
                                 void *avl_param){
    ASSOCIACAO a = (ASSOCIACAO) avl_a;
    ASSOCIACAO b = (ASSOCIACAO) avl_b;
    
    return strcmp(a->nomeClasseDestino, b->nomeClasseDestino);
}


void printAssociacao(ASSOCIACAO a){
    printf("Associacao{classe: %s | label: %s}", a->nomeClasseDestino, a->labelAssociacao);
}

