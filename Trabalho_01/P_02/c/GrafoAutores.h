#ifndef GRAFOAUTORES_H
#define GRAFOAUTORES_H
#include "avl.h"

struct s_grafoautores{
    AVL autores;
};

typedef struct s_grafoautores *GRAFOAUTORES;

GRAFOAUTORES novoGrafoAutores(void);
int nAutores(GRAFOAUTORES g);
void addCoAutoria(GRAFOAUTORES g, char *nomeAutor, char *nomeCoAutor);
void freeGrafo(GRAFOAUTORES g);
void printGrafoAutores(GRAFOAUTORES g);
void printCoAutores(GRAFOAUTORES g, char *autor);


#endif

