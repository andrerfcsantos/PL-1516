#include <stdio.h>
#include <stdlib.h>
#include "GrafoAutores.h"
#include "avl.h"
#include "CoAutoria.h"
#include "Autor.h"


GRAFOAUTORES novoGrafoAutores(){
    GRAFOAUTORES res  = (GRAFOAUTORES) malloc(sizeof(struct s_grafoautores));
    res->autores = avl_create(comparaAutorAVL, NULL, NULL);
    return res;
}

int nAutores(GRAFOAUTORES g){
    return avl_count(g->autores);
}

void addCoAutoria(GRAFOAUTORES g, char *nomeAutor, char *nomeCoAutor){
    AUTOR procuraAutor = novoAutorNome(nomeAutor);
    AUTOR procuraCoAutor = novoAutorNome(nomeCoAutor);
    AUTOR autor = avl_find(g->autores, procuraAutor);
    AUTOR coAutor = avl_find(g->autores, procuraCoAutor);
    
    if(autor!=NULL){
        //Autor ja existe, adicionar-lhe co-autor
        addCoAutor(autor, nomeCoAutor);
        free(procuraAutor);
    }else{
        //Autor nao existe, criar e adicionar coautor
        avl_insert(g->autores, procuraAutor);
        addCoAutor(procuraAutor, nomeCoAutor);
    }
    
    if(coAutor!=NULL){
        addCoAutor(coAutor, nomeAutor);
        free(procuraCoAutor);
    }else{
        avl_insert(g->autores, procuraCoAutor);
        addCoAutor(procuraCoAutor, nomeAutor);
    }
    
}

void freeGrafo(GRAFOAUTORES g){
    if(g!=NULL){
        avl_destroy(g->autores, freeAutorAVL);
    }
    free(g);
}

void printGrafoAutores(GRAFOAUTORES g){
    ITERADOR itCoAutores, itAutores = avl_new_it(g->autores);
    COAUTORIA ca;
    AUTOR a, b;
    
    while((a = avl_t_next(itAutores))!=NULL){
        printf("%s =>", a->nome);
        
        itCoAutores = avl_new_it(a->co_autorias);
        while((ca = avl_t_next(itCoAutores))!=NULL){
            printf(" %s(%d)", ca->coAutor, ca->nColaboracoes);
        }
        avl_t_free(itCoAutores);
        printf("\n");
    }
    
    avl_t_free(itAutores);
}


void printCoAutores(GRAFOAUTORES g, char *autor){
    AUTOR a = novoAutorNome(autor);
    AUTOR aut = avl_find(g->autores, a);
    COAUTORIA ca;
    ITERADOR itCoAutores = avl_new_it(aut->co_autorias);    
    printf("%s colaborou :", autor);
    while((ca = avl_t_next(itCoAutores))!=NULL){ 
        if (ca->nColaboracoes == 1)
               printf("\n\t%d vez com %s", ca->nColaboracoes, ca->coAutor);   
        else      
            printf("\n\t%d vezes com %s", ca->nColaboracoes, ca->coAutor);
    }
    printf("\n");
    avl_t_free(itCoAutores);
}

