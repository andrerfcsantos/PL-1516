#ifndef AUTOR_H
#define AUTOR_H
#include "avl.h"
#include "CoAutoria.h"

struct s_autor{
    char *nome;
    AVL co_autorias;
};

typedef struct s_autor *AUTOR;

AUTOR novoAutor(void);
AUTOR novoAutorNome(char *autor);
void addCoAutor(AUTOR a, char *coAutor);
void setNomeAutor(AUTOR a, char *novoNome);
void freeAutor(AUTOR a);
void freeAutorAVL(void *avl_item, void *avl_param);
int comparaAutorAVL(const void *avl_a, const void *avl_b, void *avl_param);



#endif

