#include <stdio.h>
#include <stdlib.h>

#include "GrafoAutores.h"

int main(int argc, char** argv) {
    GRAFOAUTORES grafo = novoGrafoAutores();
    //Jose colabora com 3 autores
    addCoAutoria(grafo, "jose", "maria");
    addCoAutoria(grafo, "jose", "manuel");
    addCoAutoria(grafo, "jose", "joana");
    addCoAutoria(grafo, "joana", "joao");
    addCoAutoria(grafo, "joana", "joao");
    addCoAutoria(grafo, "pedro", "mario");
    
    printGrafoAutores(grafo);
    freeGrafo(grafo);
    return (EXIT_SUCCESS);
}

