/* Autor.c */
AUTOR novoAutor(void);
AUTOR novoAutorNome(char *autor);
void addCoAutor(AUTOR a, char *coAutor);
void setNomeAutor(AUTOR a, char *novoNome);
void freeAutor(AUTOR a);
void freeAutorAVL(void *avl_item, void *avl_param);
int comparaAutorAVL(const void *avl_a, const void *avl_b, void *avl_param);
/* avl.c */
struct avl_table *avl_create(avl_comparison_func *compare, void *param, struct libavl_allocator *allocator);
void *avl_find(const struct avl_table *tree, const void *item);
void **avl_probe(struct avl_table *tree, void *item);
void *avl_insert(struct avl_table *table, void *item);
void *avl_replace(struct avl_table *table, void *item);
void *avl_delete(struct avl_table *tree, const void *item);
ITERADOR avl_new_it(AVL tree);
void avl_t_init(struct avl_traverser *trav, struct avl_table *tree);
void avl_t_free(ITERADOR it);
void *avl_t_first(struct avl_traverser *trav, struct avl_table *tree);
void *avl_t_last(struct avl_traverser *trav, struct avl_table *tree);
void *avl_t_find(struct avl_traverser *trav, struct avl_table *tree, void *item);
void *avl_t_insert(struct avl_traverser *trav, struct avl_table *tree, void *item);
void *avl_t_copy(struct avl_traverser *trav, const struct avl_traverser *src);
void *avl_t_next(struct avl_traverser *trav);
void *avl_t_prev(struct avl_traverser *trav);
void *avl_t_cur(struct avl_traverser *trav);
void *avl_t_replace(struct avl_traverser *trav, void *new);
struct avl_table *avl_copy(const struct avl_table *org, avl_copy_func *copy, avl_item_func *destroy, struct libavl_allocator *allocator);
void avl_destroy(struct avl_table *tree, avl_item_func *destroy);
void *avl_malloc(struct libavl_allocator *allocator, size_t size);
void avl_free(struct libavl_allocator *allocator, void *block);
void (avl_assert_insert)(struct avl_table *table, void *item);
void *(avl_assert_delete)(struct avl_table *table, void *item);
/* CoAutoria.c */
COAUTORIA novaCoAutoria(void);
COAUTORIA novaCoAutoriaNome(char *coAutor);
void addColaboracao(COAUTORIA ca);
void setCoAutor(COAUTORIA ca, char *coAutor);
void freeCoAutoria(COAUTORIA ca);
void freeCoAutoriaAVL(void *avl_item, void *avl_param);
int comparaCoAutoriaAVL(const void *avl_a, const void *avl_b, void *avl_param);
/* GrafoAutores.c */
GRAFOAUTORES novoGrafoAutores(void);
int nAutores(GRAFOAUTORES g);
void addCoAutoria(GRAFOAUTORES g, char *nomeAutor, char *nomeCoAutor);
void freeGrafo(GRAFOAUTORES g);
void printGrafoAutores(GRAFOAUTORES g);
/* testes.c */
int main(int argc, char **argv);
