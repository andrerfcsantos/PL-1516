#ifndef COAUTORIA_H
#define COAUTORIA_H

struct s_coautoria{
    char *coAutor;
    int nColaboracoes;
};

typedef struct s_coautoria *COAUTORIA;

COAUTORIA novaCoAutoria(void);
COAUTORIA novaCoAutoriaNome(char *coAutor);
void addColaboracao(COAUTORIA ca);
void setCoAutor(COAUTORIA ca, char *coAutor);
void freeCoAutoria(COAUTORIA ca);
void freeCoAutoriaAVL(void *avl_item, void *avl_param);
int comparaCoAutoriaAVL(const void *avl_a, const void *avl_b, void *avl_param);


#endif

