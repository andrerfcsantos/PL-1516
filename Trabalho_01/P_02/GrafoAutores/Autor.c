#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Autor.h"
#include "avl.h"


AUTOR novoAutor() {
    AUTOR res = (AUTOR) malloc(sizeof(struct s_autor));
    res->nome = NULL;
    res->co_autorias = avl_create(comparaCoAutoriaAVL, NULL, NULL);
    return res;
}

AUTOR novoAutorNome(char *autor) {
    AUTOR res = (AUTOR) malloc(sizeof(struct s_autor));
    res->nome = strdup(autor);
    res->co_autorias = avl_create(comparaCoAutoriaAVL, NULL, NULL);
    return res;
}


void addCoAutor(AUTOR a, char *coAutor){
    
    COAUTORIA caProcura = novaCoAutoriaNome(coAutor);
    COAUTORIA resProcura = avl_find(a->co_autorias, caProcura);
    
    if(resProcura == NULL){
        //Co-autoria nova, inserir na avl
        avl_insert(a->co_autorias, caProcura);
    }else{
        //Co-autoria ja existente, incrementar nº de co-autorias
        addColaboracao(resProcura);
        free(caProcura);
    }
    
}

void setNomeAutor(AUTOR a, char *novoNome){
    if(a != NULL){
        free(a->nome);
        a->nome = strdup(novoNome);
    }
}

void freeAutor(AUTOR a){
    if(a!=NULL){
        free(a->nome);
        avl_destroy(a->co_autorias, freeCoAutoriaAVL);
    }
    free(a);
}

void freeAutorAVL(void *avl_item, void *avl_param){
    AUTOR a  = (AUTOR) avl_item;
    freeAutor(a);
}

int comparaAutorAVL(const void *avl_a, const void *avl_b,
                                 void *avl_param){
    AUTOR a = (AUTOR) avl_a;
    AUTOR b = (AUTOR) avl_b;
    return strcmp(a->nome, b->nome);
}