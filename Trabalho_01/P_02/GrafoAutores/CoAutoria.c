#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "CoAutoria.h"
#include "avl.h"

COAUTORIA novaCoAutoria() {
    COAUTORIA res = (COAUTORIA) malloc(sizeof(struct s_coautoria));
    res->coAutor = NULL;
    res->nColaboracoes = 1;
    return res;
}

COAUTORIA novaCoAutoriaNome(char *coAutor) {
    COAUTORIA res = (COAUTORIA) malloc(sizeof(struct s_coautoria));
    res->coAutor = strdup(coAutor);
    res->nColaboracoes = 1;
    return res;
}


void addColaboracao(COAUTORIA ca){
    if(ca!=NULL)ca->nColaboracoes+=1;
}

void setCoAutor(COAUTORIA ca, char *coAutor){
    if(ca != NULL){
        free(ca->coAutor);
        ca->coAutor = strdup(coAutor);
    }
}

void freeCoAutoria(COAUTORIA ca){
    if(ca!=NULL){
        free(ca->coAutor);
    }
    free(ca);
}

void freeCoAutoriaAVL(void *avl_item, void *avl_param){
    COAUTORIA ca  = (COAUTORIA) avl_item;
    freeCoAutoria(ca);
}

int comparaCoAutoriaAVL(const void *avl_a, const void *avl_b,
                                 void *avl_param){
    COAUTORIA a = (COAUTORIA) avl_a;
    COAUTORIA b = (COAUTORIA) avl_b;
    return strcmp(a->coAutor, b->coAutor);
}




