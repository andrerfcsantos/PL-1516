#ifndef CLASSE_H
#define CLASSE_H
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "Registo.h"

struct campo
{
	char *nome;
	char *valor;

};

typedef struct campo *CAMPO;
struct registo
{
	char *categoria;
	char *chave;
	CAMPO *campos;
	char **autores;
	int nrC;
	int nrA;
	int temA;
	int temT;
	int maiorCampo;
};


typedef struct registo *REGISTO;


REGISTO newRegisto(void);
void freeRegisto(REGISTO r);
void printRegisto(REGISTO r);
char *trim(char *str);	
#endif