#include "Registo.h"
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#define MAX_CAMPOS 66
#define MAX_AUT 35

REGISTO newRegisto(){
	REGISTO r = malloc (sizeof (struct registo));
	r->nrC = 2;
	r->nrA = 0;
	r->temA = 0; 
	r->temT = 0;
	r->maiorCampo = 0;
	r->campos = malloc (MAX_CAMPOS * sizeof(CAMPO));
	int i;
	for (i=0; i < MAX_CAMPOS; i++){
		r->campos[i] = malloc(sizeof(struct campo));
		r->campos[i]->nome = NULL;	
		r->campos[i]->valor = NULL;
	}
	r->autores = malloc(MAX_AUT * sizeof(char *));
	for(i=0; i<MAX_AUT; i++)
		r->autores[i]=NULL;
	return r;
}

void freeRegisto(REGISTO r){
	int i;
	if (r!=NULL){
		free(r->categoria);
		free(r->chave);
		for(i=0; i<MAX_CAMPOS; i++)
		{	
			if((r->campos[i]) != NULL){
			free(r->campos[i]->nome);
			free(r->campos[i]->valor);
		}
		}
		for(i=0; i<MAX_AUT; i++)
			free(r->autores[i]);
		free(r->campos);
		free(r->autores);
	}
	free(r);
}
void printRegisto(REGISTO r)
{
	int w=r->maiorCampo;
    int i,j;
    printf("\n%s%s,",r->categoria, r->chave);
    if(r->temA)
    {    	
      	printf("\n\t%-*s = {%s",w,r->campos[0]->nome,trim(r->autores[0])); 
    	for (i=1; (r->autores[i]) ; i++){
    		printf(",\n");
    		for(j=0; j<w+8;j++)
    			printf(" ");
    		printf("%s",trim(r->autores[i]));
    	}
		printf("},\n");
  	}
  	if(r->temT)
  	{
  		printf("\t%-*s = {%s},\n",w,r->campos[1]->nome,r->campos[1]->valor);
  	}
  	printf("\t%-*s = {%s}",w,r->campos[2]->nome,r->campos[2]->valor);
    for(i = 3;( i < (r->nrC ) ) && r->campos[i] ; i++)
      printf(",\n\t%-*s = {%s}",w,r->campos[i]->nome,r->campos[i]->valor);
  	printf(",\n\t%-*s = {%s}",w,r->campos[r->nrC]->nome,r->campos[r->nrC]->valor);
  	printf("\n}\n");
  	
    
}

char* trim(char *str)
{
  char *end;
  while(isspace(*str)) str++;
  if(*str == 0) 
    return str;
  end = str + strlen(str) - 1;
  while(end > str && isspace(*end)) end--;
  *(end+1) = 0;
  return str;
}






