#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "categoria.h"

CATEGORIA initCat(char *nome, int count){
    CATEGORIA novaCategoria = (CATEGORIA) malloc(sizeof(struct s_cat));
    
    if(novaCategoria != NULL){
        novaCategoria->nome = strdup(nome);
        novaCategoria->count = count;
    }
    
    return novaCategoria;
}

void aumentaCounter(CATEGORIA cat, int aumento){
    if(cat != NULL){
        cat->count++;
    }
}

void freeCatAVL(void *avl_item, void *avl_param){
    if(avl_item!=NULL) free(((CATEGORIA) avl_item)->nome);
    free(avl_item);
}

int comparaCatAVL (const void *avl_a, const void *avl_b, void *avl_param){
    return strcmp(((CATEGORIA) avl_a)->nome, ((CATEGORIA) avl_b)->nome  );
}

void freeCat(CATEGORIA cat){
    if(cat!=NULL) free(cat->nome);
    free(cat);
}