#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "bloco_instr.h"
#include "arrays_dinamicos.h"


BLOCO inicializaBloco(){
    BLOCO res = (BLOCO) malloc(sizeof(struct s_bloco));
    res->instrs = ad_inicializa_str(100);
    return res;
}

BLOCO adicionaInstr(BLOCO b, char* str){
    ad_insere_elemento(b->instrs, str);
    return b;
}

BLOCO juntaBlocos(BLOCO a, BLOCO b){
    char *str;
    int i;
    int nStrings = ad_get_tamanho(b->instrs);
    
    for(i=0;i<nStrings;i++){
        str = ad_get_elemento(b->instrs, i);
        adicionaInstr(a, str);
    }
    return a;
}

char *blocoToString(BLOCO b){
    char *str, *res;
    int i=0, tamString=0;
    int nStrings = ad_get_tamanho(b->instrs);
    
    if(nStrings==0){
        return "";
    }
    
    for(i=0;i<nStrings;i++){
        str = ad_get_elemento(b->instrs,i);
        tamString += strlen(str);
    }
    
    res = (char *) calloc(tamString+1,sizeof(char));
    
    for(i=0;i<nStrings;i++){
        str = ad_get_elemento(b->instrs,i);
        strcat(res, str);
    }
    
    return res;
}
