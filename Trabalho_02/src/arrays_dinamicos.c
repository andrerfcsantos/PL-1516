#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arrays_dinamicos.h"

#define AD_ELEMS_POR_PAG_DEFAULT 15

struct array_dinamico{
    void **elementos;
    int posicao;
    int capacidade;
    ad_compara_elems *comp_func;
    ad_elimina_elems *del_func;
    struct array_dinamico* garbage_collector;
};

static ARRAY_DINAMICO ad_inicializa_gc(int);
static void ad_realloc_if_needed(ARRAY_DINAMICO);
static void quicksort(void **, ad_compara_elems *, int);


/*
 * INICIALIZACAO E LIBERTACAO MEMORIA
 */


ARRAY_DINAMICO ad_inicializa(int capacidade){
    ARRAY_DINAMICO array_d = (ARRAY_DINAMICO) malloc(sizeof(struct array_dinamico));
    
    if(array_d){
        array_d->elementos = (void **) malloc(sizeof(void *) * capacidade);
    }else{
        return NULL;
    }
    
    if(array_d->elementos != NULL){
        array_d->posicao = 0;
        array_d->capacidade = capacidade;
        array_d->garbage_collector = ad_inicializa_gc(capacidade>=10 ? capacidade/10 : 10);
    } else{
        free(array_d);
        return NULL;
    }
    
    return array_d;
}

ARRAY_DINAMICO ad_inicializa_str(int capacidade){
    return ad_inicializa_completo(capacidade, ad_comp_str, free);
}

ARRAY_DINAMICO ad_inicializa_completo(int capacidade, ad_compara_elems *comp_func, ad_elimina_elems *del_func){
    ARRAY_DINAMICO res = NULL;
    
    if(comp_func==NULL || del_func==NULL){
        return NULL;
    }else{
        res = ad_inicializa(capacidade);
    }
    
    if(res!=NULL){
        res->comp_func = comp_func;
        res->del_func = del_func;
    }
    
    return res;
}

void ad_clean_gc(ARRAY_DINAMICO ad) {
    int i;

    if (ad !=NULL & ad->del_func != NULL) {
        for (i = 0; i < ad->garbage_collector->posicao; i++)
            ad->del_func(ad->garbage_collector->elementos[i]);
    }
    
    if (ad != NULL) {
        ad_free(ad->garbage_collector);
        ad->garbage_collector = (ARRAY_DINAMICO) ad_inicializa_gc(1);
    }
}

void ad_destroy(ARRAY_DINAMICO ad) {
    int i;
    
    if (ad != NULL && ad->del_func != NULL) {
        for (i = 0; i < ad->posicao; i++)
            ad->del_func(ad->elementos[i]);

        for (i = 0; i < ad->garbage_collector->posicao; i++)
            ad->del_func(ad->garbage_collector->elementos[i]);
    }
    
    if (ad != NULL) {
        ad_free(ad->garbage_collector);
        free(ad->elementos);
    }
    
    free(ad);
}

void ad_free(ARRAY_DINAMICO ad) {
    if (ad != NULL) {
        free(ad->elementos);
        ad_free(ad->garbage_collector);
    }
    free(ad);
}

/*
 * TAMANHO E CAPACIDADE
 */

int ad_get_tamanho(ARRAY_DINAMICO ad){
    return ad->posicao;
}

int ad_get_capacidade(ARRAY_DINAMICO ad){
    return ad->capacidade;
}

/*
 * INSERCAO
 */

void ad_insere_elemento(ARRAY_DINAMICO ad, void *elemento){
    ad_realloc_if_needed(ad);
    ad->elementos[ad->posicao] = elemento;
    ad->posicao++;
}

void ad_insere_elemento_pos(ARRAY_DINAMICO ad, int pos, void *elemento){

    if (pos < ad->posicao && pos > 0) {
        ad_remove_elemento_pos(ad, pos);
        ad->elementos[pos] = elemento;
    }
}

/*
 * PESQUISA
 */

void *ad_get_elemento(ARRAY_DINAMICO ad, int pos){
    return pos < ad->posicao ? ad->elementos[pos] : NULL;
}

int ad_get_pos_elem(ARRAY_DINAMICO ad, void *elemento) {
    int encontrado = 0;
    int pos_encontrado = AD_ELEM_NAO_ENCONTRADO;
    int i = 0;

    if (ad->comp_func != NULL) {
        for (i = 0; i < ad->posicao && !encontrado; i++) {
            if (ad->comp_func(elemento, ad->elementos[i]) == 0) {
                encontrado = 1;
                pos_encontrado = i;
            }
        }
    }
    
    return pos_encontrado;
}

int ad_existe_elemento(ARRAY_DINAMICO ad, void *elemento){
    return ad_get_pos_elem(ad, elemento) == AD_ELEM_NAO_ENCONTRADO ? 0 : 1;
}


/*
 * REMOCAO
 */

void ad_remove_elemento(ARRAY_DINAMICO ad, void *elemento){
    int posicao = ad_get_pos_elem(ad,elemento);
    
    if(posicao != AD_ELEM_NAO_ENCONTRADO)
        ad_remove_elemento_pos(ad, posicao);
}

void ad_remove_elemento_pos(ARRAY_DINAMICO ad, int pos){
    int i;
    ad_insere_elemento(ad->garbage_collector,ad->elementos[pos]);
    
    for(i=pos;i<ad->posicao;i++)
        ad->elementos[i] = ad->elementos[i + 1];
    
    ad->posicao--;
}

void ad_destroi_elemento(ARRAY_DINAMICO ad, void *elemento){
    int posicao = ad_get_pos_elem(ad,elemento);
    
    if(posicao != AD_ELEM_NAO_ENCONTRADO)
        ad_destroi_elemento_pos(ad, posicao);
}

void ad_destroi_elemento_pos(ARRAY_DINAMICO ad, int pos){
    int i;
    if(ad->del_func != NULL) ad->del_func(ad->elementos[pos]);
    
    for(i=pos;i<ad->posicao;i++)
        ad->elementos[i] = ad->elementos[i + 1];
    
    ad->posicao--;
}


/*
 * ORDENACAO
 */

void ad_ordena(ARRAY_DINAMICO ad) {
    if(ad->comp_func!=NULL)
        quicksort(ad->elementos, ad->comp_func ,ad->posicao);
}

/*
 STRING
 */

int ad_comp_str(void *a, void *b){
    return strcmp((char *)a, (char *)b);
}


/*
 * FUNCOES PRIVADAS AO MODULO
 */

static ARRAY_DINAMICO ad_inicializa_gc(int capacidade){
    ARRAY_DINAMICO array_d = (ARRAY_DINAMICO) malloc(sizeof(struct array_dinamico));
    
    array_d->elementos = (void **) malloc(sizeof(void *) * capacidade);
    
    if(array_d->elementos != NULL){
        array_d->posicao = 0;
        array_d->capacidade = capacidade;
        array_d->garbage_collector = NULL;
    }
    
    return array_d;
}

static void ad_realloc_if_needed(ARRAY_DINAMICO ad){
    int nova_cap;
    void **new_ptr;
    
    if(ad->posicao == ad->capacidade){
        nova_cap = ad->capacidade*2;
        new_ptr = (void **) realloc(ad->elementos,sizeof(void *) * nova_cap);
        
        if (new_ptr != NULL){
            ad->elementos=new_ptr;
            ad->capacidade= nova_cap;
        }
    }
}

static void quicksort(void **elems, ad_compara_elems *f_comparacao, int n){
    int i, j;
    void *p, *temp;
    
    if (n < 2) return;
    p = elems[n/2];
    
    for (i = 0, j = n - 1;; i++, j--) {
        
        while(f_comparacao(elems[i],p)<0) i++;
        while(f_comparacao(p,elems[j])<0)j--;
        if (i >= j) break;
        
        temp = elems[i];
        elems[i] = elems[j];
        elems[j] = temp;
    }
    quicksort(elems,f_comparacao, i);
    quicksort(elems+i,f_comparacao, n - i);
}



