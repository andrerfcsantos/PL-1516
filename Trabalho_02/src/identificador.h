#ifndef IDENTIFICADOR_H
#define IDENTIFICADOR_H

enum e_categoria{
    PROCEDIMENTO, FUNCAO, VARIAVEL, CATEGORIA_INDEFINIDA
};

typedef enum e_categoria CATEGORIA;

enum e_tipo{
    INTEIRO, VECTOR, MATRIZ, TIPO_INDEFINIDO
};

typedef enum e_tipo TIPO;

struct s_identificador{
    char *nome;
    char *contexto;
    CATEGORIA categoria;
    TIPO tipo;
    int endereco;
    int linhas;
    int colunas;
    int nDimensoes;
    int nArgs;
};

typedef struct s_identificador *AP_IDENTIFICADOR;
typedef struct s_identificador IDENTIFICADOR;

AP_IDENTIFICADOR inicializaVariavel(char *nome, int endereco, char *contexto);
AP_IDENTIFICADOR inicializaVector(char *nome, int endereco, int tamanho, char *contexto);
AP_IDENTIFICADOR inicializaMatriz(char *nome, int endereco, int linhas, int colunas, char *contexto);
AP_IDENTIFICADOR inicializaIdProcura(char *nome, char *contexto, CATEGORIA categoria);
AP_IDENTIFICADOR inicializaFuncao(char *nome, int nargs);
AP_IDENTIFICADOR inicializaProcedimento(char *nome, int nargs);
void freeIdentificador(AP_IDENTIFICADOR id);
int comparaIdentificadores(AP_IDENTIFICADOR a, AP_IDENTIFICADOR b);
int comparaIdentificadoresGen(void *a, void *b);
void freeIdentificadorGen(void *id);
char *identificadorToString(AP_IDENTIFICADOR id);
char *getKeyIdentificador(AP_IDENTIFICADOR id);



#endif

