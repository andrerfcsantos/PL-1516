#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "avl.h"
#include "stringset.h"

static int comparaStrAVL(const void *avl_a, const void *avl_b, void *param);
static void freeStrAVL(void *str, void *param);

STRINGSET strset_inicializa(){
    STRINGSET res = (STRINGSET) malloc(sizeof(struct s_stringset));
    res->arvore = avl_create(comparaStrAVL, NULL, NULL);
    return res;
}

void strset_add(STRINGSET set, char *str){
    char *copia;
    if(str != NULL) {
        copia = strdup(str);
        if(avl_insert(set->arvore, copia)!=NULL) free(copia);
    }
}

void strset_remove(STRINGSET set, char *str){
    free(avl_delete(set->arvore, str));
}

int strset_existestr(STRINGSET set, char *str){
    return avl_find(set->arvore, str) == NULL ? 0 : 1 ;
}

char **strset_toArray(STRINGSET set){
    int i=0;
    char *str;
    int tamanho = avl_count(set->arvore);
    char **res = (char **) malloc(sizeof(char *)*(tamanho +1));
    
    TRAVERSER it =  avl_t_aInit(set->arvore);
    
    while((str=avl_t_next(it))!=NULL){
        res[i++]= strdup(str);
    }
    res[i] = NULL;
    
    avl_t_free(it);
    
    
    return res;
}

void strset_freeArray(char **a){
    int i=0;
    while(a[i]) free(a[i++]);
    free(a);
}


void strset_free(STRINGSET s){
    if(s!=NULL) avl_destroy(s->arvore, freeStrAVL);
    free(s);
}

void strset_print(STRINGSET s){
    char *str;
    TRAVERSER it =  avl_t_aInit(s->arvore);
    
    while((str=avl_t_next(it))!=NULL){
        printf("%s\n", str);
    }
    
    avl_t_free(it);
}

static int comparaStrAVL(const void *avl_a, const void *avl_b, void *param){
    char *a = (char *) avl_a;
    char *b = (char *) avl_b;
    return strcmp(a,b);
}

static void freeStrAVL(void *str, void *param){
    free(str);
}
