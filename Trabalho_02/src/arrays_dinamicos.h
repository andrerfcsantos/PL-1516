#ifndef ARRAYS_DINAMICOS_H
#define	ARRAYS_DINAMICOS_H

#define AD_ELEM_NAO_ENCONTRADO -1
#define AD_ELEM_EXISTE 1
#define AD_ELEM_NAO_EXISTE 0
#define AD_PAGINA_IMPOSSIVEL -1
#define AD_POSICAO_IMPOSSIVEL -1

typedef struct array_dinamico* ARRAY_DINAMICO;

typedef int ad_compara_elems(void *item_a, void *item_b);
typedef void ad_elimina_elems(void *item_a);

ARRAY_DINAMICO ad_inicializa(int capacidade);
ARRAY_DINAMICO ad_inicializa_str(int capacidade);
ARRAY_DINAMICO ad_inicializa_completo(int capacidade, ad_compara_elems *comp_func, ad_elimina_elems *del_func);
void ad_clean_gc(ARRAY_DINAMICO ad);
void ad_destroy(ARRAY_DINAMICO ad);
void ad_free(ARRAY_DINAMICO ad);
int ad_get_tamanho(ARRAY_DINAMICO ad);
int ad_get_capacidade(ARRAY_DINAMICO ad);
void ad_insere_elemento(ARRAY_DINAMICO ad, void *elemento);
void ad_insere_elemento_pos(ARRAY_DINAMICO ad, int pos, void *elemento);
void *ad_get_elemento(ARRAY_DINAMICO ad, int pos);
int ad_get_pos_elem(ARRAY_DINAMICO ad, void *elemento);
int ad_existe_elemento(ARRAY_DINAMICO ad, void *elemento);
void ad_remove_elemento(ARRAY_DINAMICO ad, void *elemento);
void ad_remove_elemento_pos(ARRAY_DINAMICO ad, int pos);
void ad_destroi_elemento(ARRAY_DINAMICO ad, void *elemento);
void ad_destroi_elemento_pos(ARRAY_DINAMICO ad, int pos);
void ad_ordena(ARRAY_DINAMICO ad);
int ad_comp_str(void *a, void *b);




#endif	/* ARRAYS_DINAMICOS_H */

