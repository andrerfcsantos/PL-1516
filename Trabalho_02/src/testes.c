#include <stdio.h>
#include <stdlib.h>
#include "arrays_dinamicos.h"
#include "identificador.h"

int main(int argc, char** argv) {
    int tam, i;
    char *str;
    AP_IDENTIFICADOR id;
    ARRAY_DINAMICO ad;
    AP_IDENTIFICADOR i1, i2, i3, i4, i5, i6, i7;
    
    ad = ad_inicializa_completo(100, comparaIdentificadoresGen, freeIdentificadorGen);
    
    i1 = inicializaVariavel("x", 0, "global");
    i2 = inicializaVariavel("y", 0, "global");
    i3 = inicializaVariavel("z", 0, "global");
    i4 = inicializaVariavel("contador", 0, "func");
    i5 = inicializaVariavel("i", 0, "func");
    i6 = inicializaVariavel("j", 0, "func");
    i7 = inicializaVariavel("k", 0, "func");
    
    ad_insere_elemento(ad, i1);
    ad_insere_elemento(ad, i2);
    ad_insere_elemento(ad, i3);
    ad_insere_elemento(ad, i4);
    ad_insere_elemento(ad, i5);
    ad_insere_elemento(ad, i6);
    ad_insere_elemento(ad, i7);

    tam = ad_get_tamanho(ad);
    for (i = 0; i < tam; i++) {
        id = (AP_IDENTIFICADOR) ad_get_elemento(ad, i);
        str = identificadorToString(id);
        printf("%s\n",str);
        free(str);
    }
    
    ad_destroy(ad);
    return (EXIT_SUCCESS);
}

