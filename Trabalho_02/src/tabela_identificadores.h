#ifndef TABELA_IDENTIFICADORES_H
#define TABELA_IDENTIFICADORES_H
#include "identificador.h"
#include <search.h>

#define MAX_IDENTIFICADORES 40583

typedef struct hsearch_data *AP_HASH_TAB;
typedef struct hsearch_data HASH_TAB;

struct s_tabela_identificadores{
    struct hsearch_data *htab;
};

typedef struct s_tabela_identificadores *AP_TABELA_IDENTIFICADORES;
typedef struct s_tabela_identificadores TABELA_IDENTIFICADORES;

AP_TABELA_IDENTIFICADORES inicializaTabelaIdentificadores(void);
void adicionaIdentificador(AP_TABELA_IDENTIFICADORES tab, AP_IDENTIFICADOR id);
AP_IDENTIFICADOR existeIdentificador(AP_TABELA_IDENTIFICADORES tab, AP_IDENTIFICADOR id);
void freeTabelaIdentificadores(AP_TABELA_IDENTIFICADORES tab);


#endif

