#define _GNU_SOURCE
#include <search.h>
#include <stdio.h>
#include <stdlib.h>
#include "identificador.h"
#include "tabela_identificadores.h"

static int comparaIdentificadoresAVL(const void *avl_a, const void *avl_b, void *param);
static void freeIdentificadorAVL(void *avl_item, void *avl_param);

AP_TABELA_IDENTIFICADORES inicializaTabelaIdentificadores(){
    AP_TABELA_IDENTIFICADORES res = (AP_TABELA_IDENTIFICADORES) malloc(sizeof(TABELA_IDENTIFICADORES));
    res->htab = (AP_HASH_TAB) calloc(1, sizeof(HASH_TAB));
    hcreate_r(MAX_IDENTIFICADORES, res->htab);
    return res;
}

void adicionaIdentificador(AP_TABELA_IDENTIFICADORES tab, AP_IDENTIFICADOR id){
    ENTRY e, *res;
    char *idStr = getKeyIdentificador(id);
    e.key = idStr;
    e.data = id;
    hsearch_r(e,ENTER,&res,tab->htab);
}

AP_IDENTIFICADOR existeIdentificador(AP_TABELA_IDENTIFICADORES tab,AP_IDENTIFICADOR id){
    ENTRY e, *res;
    char *idStr = getKeyIdentificador(id);
    e.key = idStr;
    hsearch_r(e, FIND, &res, tab->htab);
    free(idStr);
    return res ? (AP_IDENTIFICADOR) res->data : NULL;
}

void freeTabelaIdentificadores(AP_TABELA_IDENTIFICADORES tab){
    hdestroy_r(tab->htab);
    free(tab);
}

