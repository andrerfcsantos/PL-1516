#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* strdup, strcmp */
#include "identificador.h"


AP_IDENTIFICADOR inicializaVariavel(char *nome,  int endereco, char *contexto){
    AP_IDENTIFICADOR res = (AP_IDENTIFICADOR) malloc(sizeof(IDENTIFICADOR));
    res->nome = strdup(nome);
    res->contexto = strdup(contexto);
    res->categoria = VARIAVEL;
    res->tipo = INTEIRO;
    res->endereco = endereco;
    res->linhas = -1;
    res->colunas =-1;
    res->nDimensoes = -1;
    return res;
}

AP_IDENTIFICADOR inicializaVector(char *nome, int endereco, int tamanho, char *contexto){
    AP_IDENTIFICADOR res = (AP_IDENTIFICADOR) malloc(sizeof(IDENTIFICADOR));
    res->nome = strdup(nome);
    res->contexto = strdup(contexto);
    res->categoria = VARIAVEL;
    res->tipo = VECTOR;
    res->endereco = endereco;
    res->linhas = 1;
    res->colunas = tamanho;
    res->nDimensoes = 1;
    return res;
}

AP_IDENTIFICADOR inicializaMatriz(char *nome, int endereco, int linhas, int colunas, char *contexto){
    AP_IDENTIFICADOR res = (AP_IDENTIFICADOR) malloc(sizeof(IDENTIFICADOR));
    res->nome = strdup(nome);
    res->contexto = strdup(contexto);
    res->categoria = VARIAVEL;
    res->tipo = MATRIZ;
    res->endereco = endereco;
    res->linhas = linhas;
    res->colunas = colunas;
    res->nDimensoes = 2;
    return res;
}

AP_IDENTIFICADOR inicializaIdProcura(char *nome, char *contexto, CATEGORIA categoria){
    AP_IDENTIFICADOR res = (AP_IDENTIFICADOR) malloc(sizeof(IDENTIFICADOR));
    res->nome = strdup(nome);
    res->contexto = strdup(contexto);
    res->categoria = categoria;
    res->tipo = INTEIRO;
    res->endereco = -1;
    res->linhas = -1;
    res->colunas = -1;
    res->nDimensoes = -1;
    return res;
}

AP_IDENTIFICADOR inicializaFuncao(char *nome, int nargs){
    AP_IDENTIFICADOR res = (AP_IDENTIFICADOR) malloc(sizeof(IDENTIFICADOR));
    res->nome = strdup(nome);
    res->contexto = strdup("PROGRAM");
    res->categoria = FUNCAO;
    res->tipo = INTEIRO;
    res->endereco = -1;
    res->linhas = -1;
    res->colunas = -1;
    res->nDimensoes = -1;
    res->nArgs = nargs;
    return res;
}

AP_IDENTIFICADOR inicializaProcedimento(char *nome, int nargs){
    AP_IDENTIFICADOR res = (AP_IDENTIFICADOR) malloc(sizeof(IDENTIFICADOR));
    res->nome = strdup(nome);
    res->contexto = strdup("PROGRAM");
    res->categoria = PROCEDIMENTO;
    res->tipo = INTEIRO;
    res->endereco = -1;
    res->linhas = -1;
    res->colunas = -1;
    res->nDimensoes = -1;
    res->nArgs = nargs;
    return res;
}

void freeIdentificador(AP_IDENTIFICADOR id) {
    if (id) {
        free(id->nome);
        free(id->contexto);
    }
    free(id);
}

int comparaIdentificadores(AP_IDENTIFICADOR a, AP_IDENTIFICADOR b){
    int res_strcmp;
    
    res_strcmp = strcmp(a->nome, b->nome);
    if(res_strcmp!=0) return res_strcmp;
    res_strcmp = strcmp(a->nome, b->nome);
    if(res_strcmp!=0) return res_strcmp;
    if(a->categoria != b->categoria) return a->categoria - b->categoria;
    
    return 0;
}

int comparaIdentificadoresGen(void *a, void *b){
    AP_IDENTIFICADOR i1 = (AP_IDENTIFICADOR) a;
    AP_IDENTIFICADOR i2 = (AP_IDENTIFICADOR) b;
    return comparaIdentificadores(i1, i2);
}

void freeIdentificadorGen(void *id) {
    AP_IDENTIFICADOR i1 = (AP_IDENTIFICADOR) id;
    freeIdentificador(i1);
}

char *identificadorToString(AP_IDENTIFICADOR id){
    int res_asprintf;
    char *res;
    
    res_asprintf = asprintf(&res, "IDENTIFICADOR{nome = %s, contexto = %s, categoria = %d, tipo = %d}",
                                                id->nome, id->contexto, id->categoria, id->tipo );
    
    if(res_asprintf==-1) return NULL;
    
    return res;
}

char *getKeyIdentificador(AP_IDENTIFICADOR id){
    int res_asprintf;
    char *res;
    
    res_asprintf = asprintf(&res, "%s|%s|%d", id->nome, id->contexto, id->categoria);
    
    if(res_asprintf==-1) return NULL;
    
    return res;
}



