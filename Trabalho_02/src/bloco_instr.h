#ifndef BLOCO_INSTR_H
#define BLOCO_INSTR_H

#include "arrays_dinamicos.h"

struct s_bloco{
    ARRAY_DINAMICO instrs;
};

typedef struct s_bloco *BLOCO;

BLOCO inicializaBloco(void);
BLOCO adicionaInstr(BLOCO b, char *str);
BLOCO juntaBlocos(BLOCO a, BLOCO b);
char *blocoToString(BLOCO b);


#endif /* BLOCO_INSTR_H */

