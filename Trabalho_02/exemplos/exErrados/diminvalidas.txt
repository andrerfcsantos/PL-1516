PROGRAM {
	VAR trocas, temp;
	VAR tamanho=40;
	VAR i=0;
	VAR valores[-40];

	READ valores[i];

	while(valores[i] != -1){
		i=i+1;
		READ valores[i];
	}

	trocas=-1;
	while(trocas!=0){
		i=1;
		trocas=0;
		while(i<tamanho){
			if (valores[i - 1] > valores[i]) {
				temp = valores[i];
				valores[i] = valores[i - 1];
				valores[i - 1] = temp;
				trocas = trocas + 1;
			}
			i=i+1;
		}
	}

}


