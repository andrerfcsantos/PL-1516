%{
 #define _GNU_SOURCE
 #include <string.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include "src/tabela_identificadores.h"
 #include "src/identificador.h"
 #include "src/bloco_instr.h"
 char *strErr;
 AP_TABELA_IDENTIFICADORES tabela_identificadores;
 char *contextoActual;
 int nivel;
 int contaEnd;
 int contIf;
 int contWhile;
 int yylex();
 int yyerror(char *);

 typedef struct s_var{
    char *instr;
    char *prepAtr;
 	char *atr;
 }S_VAR;


%}

%union{
	S_VAR v;
    char *s;
    int i;
    BLOCO b;
}


%token <s> string identifier IF OTHERWISE WHILE RETURN READ PRINT VAR FUNCTION PROCEDURE PROGRAM value CALL
%type <i> init
%type <v> var
%type <s> factor term expr cond DeclVar Statement assignment Function ret MainProg
%type <b> DeclsVar MoreDeclsVar Decl Statements Decls Functions


%%



PROG         : Functions MainProg 							{printf("%s"
																	"%s",
																	$2, blocoToString($1));}
             ;


Decls        : Decls Decl 									{$$ = juntaBlocos($1, $2);}
             |												{$$ = inicializaBloco();}
             ;

Decl         : VAR DeclsVar ';'								{$$=$2;}
             ;

DeclsVar     : DeclVar MoreDeclsVar                         {$$ = adicionaInstr($2, $1);}
             ;

MoreDeclsVar : ',' DeclVar MoreDeclsVar                     {$$ = adicionaInstr($3, $2);}
             |												{$$ = inicializaBloco();}
             ;

DeclVar      : identifier init            			  		{
															
															AP_IDENTIFICADOR identificador = inicializaVariavel($1, contaEnd, contextoActual);

															if(existeIdentificador(tabela_identificadores, identificador)){
																asprintf(&strErr, "Variavel %s redeclarada.", $1);
																yyerror(strErr);
																exit(0);
															}else{
																adicionaIdentificador(tabela_identificadores, identificador);
																asprintf(&$$, "pushi %d\n", $2);
																contaEnd++;
															}
															}
			 | identifier '[' value ']'     		  		{
			 												AP_IDENTIFICADOR identificador;
			 												int tam = atoi($3);

			 												if(tam <= 0){
			 													asprintf(&strErr, "Dimensao do array %s invalida.", $1);
			 													yyerror(strErr);
			 													exit(0);
			 												}

			 												identificador = inicializaVector($1, contaEnd, tam,contextoActual);

															if(existeIdentificador(tabela_identificadores, identificador)){
																asprintf(&strErr, "Vector %s redeclarado.", $1);
																yyerror(strErr);
																exit(0);
															}else{
																adicionaIdentificador(tabela_identificadores, identificador);
																asprintf(&$$, "pushn %d\n", tam);
																contaEnd+=tam;
															}
			 												}
             | identifier '[' value ']' '[' value ']'		{
             												AP_IDENTIFICADOR identificador;
			 												int nLinhas = atoi($3);
			 												int nColunas = atoi($6);

			 												if(nLinhas <= 0 || nColunas <= 0 ){
			 													asprintf(&strErr, "Dimensao do array %s invalida.", $1);
			 													yyerror(strErr);
			 													exit(0);
			 												}

			 												identificador = inicializaMatriz($1, contaEnd, 
			 																					nLinhas, nColunas,
			 																					contextoActual);

															if(existeIdentificador(tabela_identificadores, identificador)){
																asprintf(&strErr, "Matriz %s redeclarada.", $1);
																yyerror(strErr);
																exit(0);
															}else{
																adicionaIdentificador(tabela_identificadores, identificador);
																asprintf(&$$, "pushn %d\n",nLinhas*nColunas);
																contaEnd += nLinhas*nColunas;
															}
             												}
             ;

init         :                           					{$$=0;}
             | '=' value                 					{$$=atoi($2);}
             ;


Functions    : Functions Function 							{$$ = adicionaInstr($1, $2);}
             |												{$$ = inicializaBloco();}
             ;

MainProg     : PROGRAM 										{
															nivel=0;
															contextoActual = strdup("PROGRAM");
															contaEnd=3;
															} 
					'{' Decls Statements '}'				{
															asprintf(&$$,"pushi 0\n"
															             "pushi 0\n"
															             "pushi 0\n"
																		 "%s"
																		 "start\n"
																		 "%s"
																		 "stop\n",
																		 blocoToString($4),blocoToString($5));
															}
             ;

Statements   : Statements Statement                                             {$$ = adicionaInstr($1, $2);}
             |																	{$$ = inicializaBloco();}
             ;

Statement    : IF '(' cond ')' '{' Statements '}'                               {
                                                                                asprintf(&$$, "%s"
                                                                                               "jz fim_if_%d\n"
                                                                                               "%s"
                                                                                               "fim_if_%d: \n",
                                                                                               $3, contIf, blocoToString($6), contIf);
                                                                                contIf++;
                                                                                }
             | IF '(' cond ')' '{' Statements '}' OTHERWISE '{' Statements '}'  {
                                                                                asprintf(&$$, "%s"
                                                                                               "jz else_if_%d\n"
                                                                                               "%s"
                                                                                               "jump fim_if_%d\n"
                                                                                               "else_if_%d: \n"
                                                                                               "%s"
                                                                                               "fim_if_%d: \n",$3, contIf, blocoToString($6), contIf, contIf, blocoToString($10), contIf);
                                                                                contIf++;
                                                                                }
             | assignment ';'                                                   {$$=$1;}
             | WHILE '(' cond ')' '{' Statements '}'                            {
                                                                                asprintf(&$$, "cond_while_%d:\n"
                                                                                              "%s"
                                                                                              "jz fim_while_%d\n"
                                                                                              "%s"
                                                                                              "jump cond_while_%d\n"
                                                                                              "fim_while_%d:\n",
                                                                                              contWhile, $3, contWhile, blocoToString($6),contWhile, contWhile);
                                                                                contWhile++;
                                                                                }
             | identifier '(' ')' ';'                                           {
             																	char *err;
             																	AP_IDENTIFICADOR pf, pp;
             																	AP_IDENTIFICADOR res_pf, res_pp;

             																	pp = inicializaIdProcura($1, "PROGRAM",PROCEDIMENTO);
             																	pf = inicializaIdProcura($1, "PROGRAM",FUNCAO);

             																	res_pp = existeIdentificador(tabela_identificadores, pp);
             																	res_pf = existeIdentificador(tabela_identificadores, pf);

             																	if(res_pp!=NULL && res_pp->nArgs==0){
             																		asprintf(&$$, "pusha proc_%s\n"
             																					  "call\n"
             																					  "nop\n",$1);
             																	}else{
             																		if(res_pf!=NULL && res_pf->nArgs==0){
             																		asprintf(&$$, "pusha func_%s\n"
             																					  "call\n"
             																					  "nop\n",$1);
             																		}else{
             																		asprintf(&err,"\"%s\" nao corresponde a uma função ou procedimento ou foi passada com nº de argumenros invalido.", $1);
             																		yyerror(err);
             																		}
             																	}
             																	}
             | identifier '(' expr ')' ';'                                      {
             																	char *err;
             																	AP_IDENTIFICADOR pf, pp;
             																	AP_IDENTIFICADOR res_pf, res_pp;

             																	pp = inicializaIdProcura($1, "PROGRAM",PROCEDIMENTO);
             																	pf = inicializaIdProcura($1, "PROGRAM",FUNCAO);

             																	res_pp = existeIdentificador(tabela_identificadores, pp);
             																	res_pf = existeIdentificador(tabela_identificadores, pf);

             																	if(res_pp!=NULL && res_pp->nArgs==1){
             																		asprintf(&$$, "%s"
             																					  "pushg 1\n"
             																					  "pusha proc_%s\n"
             																					  "call\n"
             																					  "nop\n",$3, $1);
             																	}else{
             																		if(res_pf!=NULL && res_pf->nArgs==1){
             																			asprintf(&$$, "%s"
             																					      "pushg 1\n"
             																					      "pusha func_%s\n"
             																					      "call func_%s\n"
             																					      "nop\n",$3, $1);
             																		}else{
             																		asprintf(&err,"\"%s\" nao corresponde a uma função ou procedimento ou foi passada com nº de argumenros invalido.", $1);
             																		yyerror(err);
             																		}
             																	}
             																	}
             | identifier '(' expr ',' expr ')' ';'                             {
             																	char *err;
             																	AP_IDENTIFICADOR pf, pp;
             																	AP_IDENTIFICADOR res_pf, res_pp;

             																	pp = inicializaIdProcura($1, "PROGRAM",PROCEDIMENTO);
             																	pf = inicializaIdProcura($1, "PROGRAM",FUNCAO);

             																	res_pp = existeIdentificador(tabela_identificadores, pp);
             																	res_pf = existeIdentificador(tabela_identificadores, pf);

             																	if(res_pp!=NULL && res_pp->nArgs==2){
             																		asprintf(&$$, "%s"
             																					  "pushg 1\n"
             																					  "%s"
             																					  "pushg 2\n"
             																					  "pusha proc_%s\n"
             																					  "call\n"
             																					  "nop\n",$3, $5, $1);
             																	}else{
             																		if(res_pf!=NULL && res_pf->nArgs==2){
             																			asprintf(&$$, "%s"
             																					  "pushg 1\n"
             																					  "%s"
             																					  "pushg 2\n"
             																					  "pusha func_%s\n"
             																					  "call\n"
             																					  "nop\n",$3, $5, $1);
             																		}else{
             																		asprintf(&err,"\"%s\" nao corresponde a uma função ou procedimento ou foi passada com nº de argumenros invalido.", $1);
             																		yyerror(err);
             																		}
             																	}}
             | PRINT expr ';'                                                   {asprintf(&$$, "%s"
                                                                                               "writei\n",$2);}
             | PRINT string ';'                                                 {asprintf(&$$, "pushs %s\n"
                                                                                               "writes\n",$2);}
             | READ var ';'                                                     {asprintf(&$$, "%s"
                                                                                               "read\n"
                                                                                               "atoi\n"
                                                                                               "%s",$2.prepAtr, $2.atr);}
             ;

Function     : FUNCTION identifier '(' ')'
																				{
																				AP_IDENTIFICADOR identificador;
																				identificador = inicializaFuncao($2,0);
																				adicionaIdentificador(tabela_identificadores, identificador);
																				contaEnd = 0;
																				nivel =1;
																				contextoActual = strdup($2);
																				} 
				'{' Decls Statements ret '}'								{
																				asprintf(&$$, "func_%s: \n"
																							  "nop\n"
																							  "%s"
																							  "%s"
																							  "%s",
																							  $2,blocoToString($7), blocoToString($8), $9);
																				}
			 | FUNCTION identifier '(' identifier ')'							{
																				AP_IDENTIFICADOR identificador,a1;
																				identificador = inicializaFuncao($2,1);
																				a1 = inicializaVariavel($4, 0, $2);
																				adicionaIdentificador(tabela_identificadores, identificador);
																				adicionaIdentificador(tabela_identificadores, a1);
																				contaEnd = 1;
																				nivel =1;
																				contextoActual = strdup($2);
																				} 
				'{' Decls Statements ret '}'								{
																				asprintf(&$$, "func_%s: \n"
																							  "nop\n"
																							  "pushg 1\n"
																							  "%s"
																							  "%s"
																							  "%s",
																							  $2,blocoToString($8), blocoToString($9), $10);
																				}
			 | FUNCTION identifier '(' identifier ',' identifier ')'			{
																				AP_IDENTIFICADOR identificador,a1,a2;
																				identificador = inicializaFuncao($2,2);
																				a1 = inicializaVariavel($4, 0, $2);
																				a2 = inicializaVariavel($6, 1, $2);
																				adicionaIdentificador(tabela_identificadores, identificador);
																				adicionaIdentificador(tabela_identificadores, a1);
																				adicionaIdentificador(tabela_identificadores, a2);
																				contaEnd = 2;
																				nivel =1;
																				contextoActual = strdup($2);
																				} 
				'{' Decls Statements ret '}'								{
																				asprintf(&$$, "func_%s: \n"
																							  "nop\n"
																							  "pushg 1\n"
																							  "pushg 2\n"
																							  "%s"
																							  "%s"
																							  "%s",
																							  $2,blocoToString($10), blocoToString($11), $12);
																				}




             | PROCEDURE identifier '(' ')'
																				{
																				AP_IDENTIFICADOR identificador;
																				identificador = inicializaProcedimento($2,0);
																				adicionaIdentificador(tabela_identificadores, identificador);
																				contaEnd = 0;
																				nivel =1;
																				contextoActual = strdup($2);
																				} 
				'{' Decls Statements '}'										{
																				asprintf(&$$, "func_%s: \n"
																							  "nop\n"
																							  "%s"
																							  "%s"
                                                                                              "return\n"
																							  ,$2,blocoToString($7), blocoToString($8));
																				}
			 | PROCEDURE identifier '(' identifier ')'							{
																				AP_IDENTIFICADOR identificador,a1;
																				identificador = inicializaProcedimento($2,1);
																				a1 = inicializaVariavel($4, 0, $2);
																				adicionaIdentificador(tabela_identificadores, identificador);
																				adicionaIdentificador(tabela_identificadores, a1);
																				contaEnd = 1;
																				nivel =1;
																				contextoActual = strdup($2);
																				} 
				'{' Decls Statements '}'								{
																				asprintf(&$$, "func_%s: \n"
																							  "nop\n"
																							  "pushg 1\n"
																							  "%s"
																							  "%s"
                                                                                              "return\n"
																							  ,$2,blocoToString($8), blocoToString($9));
																				}
			 | PROCEDURE identifier '(' identifier ',' identifier ')'			{
																				AP_IDENTIFICADOR identificador,a1,a2;
																				identificador = inicializaProcedimento($2,2);
																				a1 = inicializaVariavel($4, 0, $2);
																				a2 = inicializaVariavel($6, 1, $2);
																				adicionaIdentificador(tabela_identificadores, identificador);
																				adicionaIdentificador(tabela_identificadores, a1);
																				adicionaIdentificador(tabela_identificadores, a2);
																				contaEnd = 2;
																				nivel =1;
																				contextoActual = strdup($2);
																				}
				'{' Decls Statements '}'										{
																				asprintf(&$$, "func_%s: \n"
																							  "nop\n"
																							  "pushg 1\n"
																							  "pushg 2\n"
																							  "%s"
																							  "%s"
                                                                                              "return\n"
																							  ,$2,blocoToString($10), blocoToString($11));
																				}
             ;

ret      	 : RETURN expr ';'								{asprintf(&$$, "%s"
																		   "storeg 0\n"
																		   "return\n",
																		   $2);}
             ;

assignment   : var '=' expr                                 {asprintf(&$$,"%s"
																		  "%s"
																		  "%s",
																		  $1.prepAtr, $3, $1.atr);}
             ;

var          : identifier									{
															AP_IDENTIFICADOR id, rp;

															id = inicializaIdProcura($1,contextoActual, VARIAVEL);
															
															if(!(rp=existeIdentificador(tabela_identificadores, id))){
																if(nivel==1){
																	//Varivel nao encontrada, tenta procurar nas vars globais
																	id = inicializaIdProcura($1,"PROGRAM", VARIAVEL);

																	if(!(rp = existeIdentificador(tabela_identificadores, id))){
																		asprintf(&strErr, "Variável %s usada mas não declarada.", $1);
																		yyerror(strErr);
																		exit(0);
																	}else{
																	//Variavel encontrada, mas apenas no contexto global
																	asprintf(&$$.instr,"pushg %d\n" , rp->endereco);
																	asprintf(&$$.atr,"storeg %d\n" , rp->endereco);
                                                                    $$.prepAtr = "";
																	}
																}else{
																	//Contexto é o global e var não foi encontrada
																	asprintf(&strErr, "Variável %s usada mas não declarada.", $1);
																	yyerror(strErr);
																	exit(0);
																}
															}else{
																//Variavel encontrada no contexto actual
																if(nivel==0){
																	asprintf(&$$.instr,"pushg %d\n" , rp->endereco);
																	asprintf(&$$.atr,"storeg %d\n" , rp->endereco);
                                                                    $$.prepAtr = "";
																}else{
																	asprintf(&$$.instr,"pushl %d\n" , rp->endereco);
																	asprintf(&$$.atr,"storel %d\n" , rp->endereco);
                                                                    $$.prepAtr = "";
																}
															}

															}


             | identifier '[' expr ']'						{
             												AP_IDENTIFICADOR id, rp;

															id = inicializaIdProcura($1,contextoActual, VARIAVEL);
															
															if(!(rp=existeIdentificador(tabela_identificadores, id))){
																if(nivel==1){
																	//Varivel nao encontrada, tenta procurar nas vars globais
																	id = inicializaIdProcura($1,"PROGRAM", VARIAVEL);

																	if(!(rp=existeIdentificador(tabela_identificadores, id))){
																		asprintf(&strErr, "Vector %s usado mas não declarada.", $1);
																		yyerror(strErr);
																		exit(0);
																	}else{
																	//Variavel encontrada, mas apenas no contexto global
																	asprintf(&$$.instr,"pushgp\n"
																					   "pushi %d\n"
																					   "padd\n"
																					   "%s"
																					   "loadn\n" , rp->endereco, $3);

                                                                    asprintf(&$$.prepAtr,"pushgp\n"
                                                                                         "pushi %d\n"
                                                                                         "padd\n"
                                                                                         "%s" , rp->endereco, $3);
                                                                    asprintf(&$$.atr,"storen\n");

																	}
																}else{
																	//Contexto é o global e var não foi encontrada
																	asprintf(&strErr, "Vector %s usada mas não declarada.", $1);
																	yyerror(strErr);
																	exit(0);
																}
															}else{
																//Variavel encontrada no contexto actual
																if(nivel==0){
																	asprintf(&$$.instr,"pushgp\n"
																					   "pushi %d\n"
																					   "padd\n"
																					   "%s"
																					   "loadn\n" , rp->endereco, $3);

                                                                    asprintf(&$$.prepAtr,"pushgp\n"
                                                                                         "pushi %d\n"
                                                                                         "padd\n"
                                                                                         "%s" , rp->endereco, $3);
                                                                    asprintf(&$$.atr,"storen\n");

																}else{
																	asprintf(&$$.instr,"pushfp\n"
																					   "pushi %d\n"
																					   "padd\n"
																					   "%s"
																					   "loadn\n" , rp->endereco, $3);

                                                                    asprintf(&$$.prepAtr,"pushfp\n"
                                                                                         "pushi %d\n"
                                                                                         "padd\n"
                                                                                         "%s" , rp->endereco, $3);
                                                                    asprintf(&$$.atr,"storen\n");
																}
															}
             												}
             | identifier '[' expr ']' '[' expr ']'			{
             												AP_IDENTIFICADOR id, rp;
             												int tamMat;
															id = inicializaIdProcura($1, contextoActual, VARIAVEL);
															
															if(!(rp=existeIdentificador(tabela_identificadores, id))){
																if(nivel==1){
																	//Varivel nao encontrada, tenta procurar nas vars globais
																	id = inicializaIdProcura($1,"PROGRAM", VARIAVEL);

																	if(!(rp=existeIdentificador(tabela_identificadores, id))){
																		asprintf(&strErr, "Vector %s usado mas não declarada.", $1);
																		yyerror(strErr);
																		exit(0);
																	}else{

																	asprintf(&$$.instr,"pushgp\n"
                                                                                       "pushi %d\n"
                                                                                       "padd\n"
                                                                                       "%s"
                                                                                       "pushi %d\n"
                                                                                       "mul\n"
                                                                                       "%s"
                                                                                       "add\n" 
                                                                                       "loadn\n" , rp->endereco, $3, rp->colunas, $6);

                                                                    asprintf(&$$.prepAtr,"pushgp\n"
                                                                                         "pushi %d\n"
                                                                                         "padd\n"
                                                                                         "%s"
                                                                                         "pushi %d\n"
                                                                                         "mul\n"
                                                                                         "%s"
                                                                                         "add\n" , rp->endereco, $3, rp->colunas, $6);
                                                                    asprintf(&$$.atr,"storen\n");
																	}
																}else{
																	//Contexto é o global e var não foi encontrada
																	asprintf(&strErr, "Vector %s usada mas não declarada.", $1);
																	yyerror(strErr);
																	exit(0);
																}
															}else{
																//Variavel encontrada no contexto actual
																if(nivel==0){
																	asprintf(&$$.instr,"pushgp\n"
                                                                                       "pushi %d\n"
                                                                                       "padd\n"
                                                                                       "%s"
                                                                                       "pushi %d\n"
                                                                                       "mul\n"
                                                                                       "%s"
                                                                                       "add\n" 
                                                                                       "loadn\n" , rp->endereco, $3, rp->colunas, $6);

                                                                    asprintf(&$$.prepAtr,"pushgp\n"
                                                                                         "pushi %d\n"
                                                                                         "padd\n"
                                                                                         "%s"
                                                                                         "pushi %d\n"
                                                                                         "mul\n"
                                                                                         "%s"
                                                                                         "add\n" , rp->endereco, $3, rp->colunas, $6);
                                                                    asprintf(&$$.atr,"storen\n");
																}else{
																	asprintf(&$$.instr,"pushfp\n"
                                                                                       "pushi %d\n"
                                                                                       "padd\n"
                                                                                       "%s"
                                                                                       "pushi %d\n"
                                                                                       "mul\n"
                                                                                       "%s"
                                                                                       "add\n" 
                                                                                       "loadn\n" , rp->endereco, $3, rp->colunas, $6);

                                                                    asprintf(&$$.prepAtr,"pusfp\n"
                                                                                         "pushi %d\n"
                                                                                         "padd\n"
                                                                                         "%s"
                                                                                         "pushi %d\n"
                                                                                         "mul\n"
                                                                                         "%s"
                                                                                         "add\n" , rp->endereco, $3, rp->colunas, $6);
                                                                    asprintf(&$$.atr,"storen\n");
																}
															}

             												}
             ;


cond         : expr '>' '=' expr 		{asprintf(&$$, "%s%ssupeq\n", $1, $4);}
             | expr '<' '=' expr 		{asprintf(&$$, "%s%sinfeq\n", $1, $4);}
             | expr '=' '=' expr 		{asprintf(&$$, "%s%sequal\n", $1, $4);}
             | expr '!' '=' expr 		{asprintf(&$$, "%s%sequal\nnot\n", $1, $4);}
             | expr '>' expr 			{asprintf(&$$, "%s%ssup\n", $1, $3);}
             | expr '<' expr 			{asprintf(&$$, "%s%sinf\n", $1, $3);}
             | expr
             ;

expr         : term 					{$$=$1;}
             | expr '+' term 			{asprintf(&$$, "%s%sadd\n", $1, $3);}
             | expr '-' term 			{asprintf(&$$, "%s%ssub\n", $1, $3);}
             | expr '|' term 			{asprintf(&$$, "%s%sadd\n", $1, $3);}
             ;

term         : factor					{$$=$1;}
             | term '*' factor			{asprintf(&$$, "%s%smul\n", $1, $3);}
             | term '/' factor			{asprintf(&$$, "%s%sdiv\n", $1, $3);}
             | term '%' factor			{asprintf(&$$, "%s%smod\n", $1, $3);}
             | term '&' factor			{asprintf(&$$, "%s%smul\n", $1, $3);}
             ;

factor       : value					{asprintf(&$$, "pushi %s\n",$1);}
             | var 						{$$=$1.instr;}
             | '(' cond ')'				{$$=$2;}
             | identifier '(' ')'		{
             							char *err;
             							AP_IDENTIFICADOR pf, res_pf;
             							pf = inicializaIdProcura($1, "PROGRAM", FUNCAO);         							
             							res_pf = existeIdentificador(tabela_identificadores, pf);
             							if(res_pf!=NULL && res_pf->nArgs==0){
             								asprintf(&$$, "pusha func_%s\n"
             											  "call\n"
             											  "nop\n"
             											  "pushg 0\n"
             											  ,$1);
             							}else{
             								asprintf(&err,"\"%s\" nao corresponde a uma função ou procedimento ou foi passada com nº de argumenros invalido.", $1);
             								yyerror(err);
             							}


             							}
             | identifier '(' expr ')'	{
             							char *err;
             							AP_IDENTIFICADOR pf, res_pf;
             							pf = inicializaIdProcura($1, "PROGRAM", FUNCAO);         							
             							res_pf = existeIdentificador(tabela_identificadores, pf);

             							if(res_pf!=NULL && res_pf->nArgs==1){
             								asprintf(&$$, "%s"
             											  "storeg 1\n"
             											  "pusha func_%s\n"
             											  "call\n"
             											  "nop\n"
             											  "pushg 0\n"
             											  ,$3, $1);
             							}else{
             								asprintf(&err,"\"%s\" nao corresponde a uma função ou procedimento ou foi passada com nº de argumenros invalido.", $1);
             								yyerror(err);
             							}


             							}
             | identifier '(' expr ',' expr ')'		{
             										char *err;
             										AP_IDENTIFICADOR pf, res_pf;
             										pf = inicializaIdProcura($1, "PROGRAM", FUNCAO);         							
             										res_pf = existeIdentificador(tabela_identificadores, pf);

             										if(res_pf!=NULL && res_pf->nArgs==2){
             											asprintf(&$$, "%s"
             											  			  "storeg 1\n"
             											  			  "%s"
             											  			  "storeg 2\n"
             											  			  "pusha func_%s\n"
             											  			  "call\n"
             											  			  "nop\n"
             											  			  "pushg 0\n"
             											  			  ,$3, $5, $1);
             										}else{
             											asprintf(&err,"\"%s\" nao corresponde a uma função ou procedimento ou foi passada com nº de argumenros invalido.", $1);
             											yyerror(err);
             										}

             										}
             ;

%%

#include "ling.yy.c"

int yyerror(char *s){
    fprintf(stderr, "Erro sintatico: %s (linha: %d | char: %d)\n", s, yylineno, yychar);
    return 1;
}

int main(){
	contextoActual = NULL;
	contaEnd =0;
    contIf=0;
    contWhile=0;
	tabela_identificadores = inicializaTabelaIdentificadores();
    yyparse();
    return 0;
}